# defmodule Aoc do
#   def my_count(), do: my_count(1, 0)

#   def my_count(n, acc) when n < 30_000_000 do
#     my_count(n + 1, acc + 1)
#   end

#   def my_count(_, acc), do: acc
# end

# Aoc.my_count()
# |> IO.inspect()
1..30_000_000
|> Enum.count(fn n ->
  n > 20_000_000
end)
|> IO.inspect()
