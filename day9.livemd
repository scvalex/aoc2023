# AoC 2023 - Day 9

```elixir
Mix.install([
  {:kino, "~> 0.11.3"}
])
```

## Helpers

```elixir
example1 = Kino.Input.textarea("Example")
```

```elixir
full = Kino.Input.textarea("Full")
```

```elixir
defmodule Aoc do
  def read_input(%Kino.Input{} = input) do
    Kino.Input.read(input)
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      line
      |> String.split(" ")
      |> Enum.map(&String.to_integer/1)
    end)
  end
end
```

## Part 1

```elixir
defmodule Part1 do
  def next_value(xs) when is_list(xs) do
    if Enum.all?(xs, &(&1 == 0)) do
      0
    else
      sub_last_digit = next_value(diffs(xs))
      List.last(xs) + sub_last_digit
    end
  end

  def diffs(xs) when is_list(xs) do
    xs
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.map(fn [a, b] -> b - a end)
  end
end

Aoc.read_input(full)
|> Enum.map(&Part1.next_value/1)
|> Enum.sum()
```

## Part 2

```elixir
defmodule Part2 do
  def prev_value(xs) when is_list(xs) do
    if Enum.all?(xs, &(&1 == 0)) do
      0
    else
      List.first(xs) - prev_value(Part1.diffs(xs))
    end
  end
end

Aoc.read_input(full)
|> Enum.map(&Part2.prev_value/1)
|> Enum.sum()
```

<!-- livebook:{"branch_parent_index":0} -->

## Experiments

```elixir
[3, 6, 9, 12, 15]
|> Enum.chunk_every(2, 1, :discard)
|> Enum.map(fn [a, b] -> b - a end)
```
