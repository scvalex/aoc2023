AoC 2023
========

> Solutions to [Advent of Code](https://adventofcode.com/) 2023 in [Livebook](https://livebook.dev)

Live-stream VODs of my writing these solutions are available on
[YouTube][1] ([playlist][2]).

[1]: https://www.youtube.com/@scvalex
[2]: https://youtube.com/playlist?list=PL2DxwWG9IY3ZqpSS8BLQyKmqvIPSMjdop&feature=shared
