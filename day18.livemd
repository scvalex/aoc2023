# AoC 2023 - Day 18

```elixir
Mix.install([
  {:kino, "~> 0.12.0"},
  {:phoenix_live_view, "~> 0.20.1"}
])
```

## Inputs

```elixir
example = Kino.Input.textarea("Example")
```

```elixir
example2 = Kino.Input.textarea("Example")
```

```elixir
full = Kino.Input.textarea("Full")
```

## Part 1

```elixir
defmodule Part1 do
  def read_input(%Kino.Input{} = input) do
    instructions =
      Kino.Input.read(input)
      |> String.split("\n", trim: true)
      |> Enum.map(fn line ->
        [dir, len, col] = line |> String.split(" ")

        dir =
          case dir do
            "D" -> :down
            "L" -> :left
            "U" -> :up
            "R" -> :right
          end

        len = String.to_integer(len)
        <<?(, col::binary-size(7), ?)>> = col
        %{dir: dir, len: len, col: col}
      end)

    %{instructions: instructions}
    |> to_lines()
    |> compute_extents()
    |> compute_edges()
  end

  def to_lines(%{instructions: instructions} = data) do
    lines =
      instructions
      |> Enum.scan(%{x1: 0, y1: 0, x2: 0, y2: 0}, fn %{col: col, dir: dir, len: len}, prev ->
        {x2, y2} =
          case dir do
            :left -> {prev.x2 - len, prev.y2}
            :right -> {prev.x2 + len, prev.y2}
            :up -> {prev.x2, prev.y2 - len}
            :down -> {prev.x2, prev.y2 + len}
          end

        %{x1: prev.x2, y1: prev.y2, x2: x2, y2: y2, col: col}
      end)

    Map.put(data, :lines, lines)
  end

  def compute_extents(%{lines: lines} = data) do
    {xs, ys} =
      lines
      |> Enum.map(fn %{x1: x1, y1: y1, x2: x2, y2: y2} ->
        {[x1, x2], [y1, y2]}
      end)
      |> Enum.reduce({[], []}, fn {xs, ys}, {acc_xs, acc_ys} ->
        {[xs | acc_xs], [ys | acc_ys]}
      end)

    xs = List.flatten(xs)
    ys = List.flatten(ys)

    data
    |> Map.put(:min_x, Enum.min(xs))
    |> Map.put(:max_x, Enum.max(xs))
    |> Map.put(:min_y, Enum.min(ys))
    |> Map.put(:max_y, Enum.max(ys))
  end

  def compute_filled_in_edges(%{lines: lines} = data) do
    filled_in_edges =
      lines
      |> Enum.map(fn line ->
        abs(line.x2 - line.x1) + abs(line.y2 - line.y1)
      end)
      |> Enum.sum()

    Map.put(data, :filled_in_edges, filled_in_edges)
  end

  def compute_edges(%{instructions: instructions} = data) do
    edges =
      instructions
      |> Enum.scan({0.5, 0.5}, fn %{dir: dir, len: len}, {x, y} ->
        case dir do
          :left -> {x - len, y}
          :right -> {x + len, y}
          :up -> {x, y - len}
          :down -> {x, y + len}
        end
      end)
      |> then(fn points -> [{0.5, 0.5} | points] end)
      |> Enum.chunk_every(2, 1, :discard)
      |> Enum.map(fn [{x1, y1}, {x2, y2}] ->
        %{x1: x1, y1: y1, x2: x2, y2: y2}
      end)

    Map.put(data, :edges, edges)
  end

  def shoelace(%{edges: edges, filled_in_edges: filled_in_edges} = data) do
    shoelace_2 =
      edges
      |> Enum.map(fn edge ->
        edge.x1 * edge.y2 - edge.x2 * edge.y1
      end)
      |> Enum.sum()

    area = shoelace_2 / 2 + filled_in_edges / 2 + 1

    Map.put(data, :area, area)
  end
end

Part1.read_input(example2)
|> Part1.compute_filled_in_edges()
|> Part1.shoelace()
|> then(fn data ->
  data.area
end)

# 52761 too high
# 35991 correct
```

## Part 2

```elixir
defmodule Part2 do
  def read_input(%Kino.Input{} = input) do
    instructions =
      Kino.Input.read(input)
      |> String.split("\n", trim: true)
      |> Enum.map(fn line ->
        [_, _, col] = line |> String.split(" ")
        <<?(, ?#, len::binary-size(5), dir, ?)>> = col
        <<?(, ?#, col::binary-size(6), ?)>> = col
        len = String.to_integer(len, 16)

        dir =
          case dir do
            ?0 -> :right
            ?1 -> :down
            ?2 -> :left
            ?3 -> :up
          end

        %{dir: dir, len: len, col: col}
      end)

    %{instructions: instructions}
    |> Part1.to_lines()
    |> Part1.compute_extents()
    |> Part1.compute_edges()
  end
end

Part2.read_input(full)
|> Part1.compute_filled_in_edges()
|> Part1.shoelace()
|> then(fn data ->
  data.area
end)

# 54058824661845 correct
```

## Visualization

```elixir
defmodule Vis do
  import Phoenix.Component

  def draw(data) do
    dim = 900
    width = data.max_x - data.min_x + 1
    height = data.max_y - data.min_y + 1
    side = dim / max(width, height)

    assigns = %{
      width: side * width,
      height: side * height,
      side: side,
      data: data
    }

    template = ~H"""
      <svg width={@width} height={@height} viewBox={"#{@data.min_x * @side - 20} #{@data.min_y * @side - 20} #{@width + 40} #{@height + 40}"} xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <rect fill="#000" x={@data.min_x * @side - 20} y={@data.min_y * @side - 20} width={@width + 40} height={@height + 40} />
        <.grid data={@data} side={@side} />
        <.outline data={@data} side={@side} />
        <.edges data={@data} side={@side} />
      </svg>
    """

    template
    |> Phoenix.LiveViewTest.rendered_to_string()
  end

  def grid(assigns) do
    ~H"""
      <line
        :for={y <- @data.min_y..(@data.max_y + 1)}
        stroke="#111"
        stroke-width="1"
        x1={@data.min_x * @side}
        y1={y * @side}
        x2={@data.max_x * @side}
        y2={y * @side}
        />
      <line
        :for={x <- @data.min_x..(@data.max_x + 1)}
        stroke="#111"
        stroke-width="1"
        x1={x * @side}
        y1={@data.min_y * @side}
        x2={x * @side}
        y2={@data.max_y * @side}
        />
    """
  end

  def outline(assigns) do
    ~H"""
      <rect
        :for={line <- @data.lines}
        stroke={line.col}
        stroke-width="2"
        x={min(line.x1, line.x2) * @side}
        y={min(line.y1, line.y2) * @side}
        width={(abs(line.x2 - line.x1) + 1) * @side}
        height={(abs(line.y2 - line.y1) + 1) * @side}
      />
    """
  end

  def edges(assigns) do
    ~H"""
      <line
        :for={edge <- @data.edges}
        stroke="#f00"
        stroke-width="1"
        x1={edge.x1 * @side}
        y1={edge.y1 * @side}
        x2={edge.x2 * @side}
        y2={edge.y2 * @side}
        />
    """
  end

  def shoelace(%{edges: edges} = data) do
    shoelace_2 =
      edges
      |> Enum.map(fn edge ->
        edge.x1 * edge.y2 - edge.x2 * edge.y1
      end)
      |> Enum.sum()

    Map.put(data, :shoelace, shoelace_2 / 2)
  end
end

Part1.read_input(example)
|> Part1.compute_filled_in_edges()
|> Part1.shoelace()
|> Vis.draw()
|> Kino.Shorts.html()
```
