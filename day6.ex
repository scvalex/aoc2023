'''
Time:        38     94     79     70
Distance:   241   1549   1074   1091
'''
|> List.to_string()
|> String.split("\n", trim: true)
|> Enum.map(fn line ->
  line
  |> String.split(":")
  |> then(fn [_, nums] -> nums end)
  |> String.split(" ", trim: true)
  |> Enum.join()
  |> String.to_integer()
end)
|> List.to_tuple()
|> then(fn {race_time, record_dist} ->
  1..(race_time - 1)
  |> Enum.count(fn time_button_held ->
    if rem(time_button_held, 1_000_000) == 0 do
      IO.puts("#{time_button_held} / #{race_time}")
    end
    time_to_sail = race_time - time_button_held
    speed = time_button_held
    time_to_sail * speed > record_dist
  end)
end)
|> IO.inspect()
