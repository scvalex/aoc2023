# AoC 2023 - Day 20

```elixir
Mix.install([
  {:kino, "~> 0.12.0"}
])
```

## Inputs

```elixir
example1 = Kino.Input.textarea("Example")
```

```elixir
example2 = Kino.Input.textarea("Example")
```

```elixir
full = Kino.Input.textarea("Full")
```

## Part 1

```elixir
defmodule Part1 do
  def read_input(%Kino.Input{} = input) do
    modules =
      Kino.Input.read(input)
      |> String.split("\n", trim: true)
      |> Enum.map(fn line ->
        line
        |> String.split(" -> ")
        |> then(fn [name, outputs] ->
          outputs = outputs |> String.split(", ")

          {name, type} =
            case name do
              <<?%, name::binary>> -> {name, :flip_flop}
              <<?&, name::binary>> -> {name, :conjunctor}
              "broadcaster" -> {name, :broadcaster}
              _ -> {name, :normal}
            end

          {name, %{outputs: outputs, type: type, inputs: []}}
        end)
      end)
      |> Enum.into(%{})

    modules =
      modules
      |> Enum.reduce(modules, fn {name, %{outputs: outputs}}, modules ->
        outputs
        |> Enum.reduce(modules, fn output, modules ->
          Map.update(
            modules,
            output,
            %{outputs: [], type: :normal, inputs: [name]},
            fn output_module ->
              %{output_module | inputs: [name | output_module.inputs]}
            end
          )
        end)
      end)

    %{modules: modules}
  end

  def initial_state(%{modules: modules}) do
    state =
      modules
      |> Enum.map(fn {name, %{inputs: inputs, type: type}} ->
        {name, initial_state(type, inputs)}
      end)
      |> Enum.into(%{})

    %{pulses: %{true => 0, false => 0, :rx => 0}, state: state}
  end

  def initial_state(:normal, _), do: nil
  def initial_state(:broadcaster, _), do: nil
  def initial_state(:flip_flop, _), do: false

  def initial_state(:conjunctor, inputs) do
    inputs
    |> Enum.map(fn input -> {input, false} end)
    |> Enum.into(%{})
  end

  def simulate1(%{pulses: _, state: _} = input, %{modules: _} = data) do
    run_simulation(
      [{"broadcaster", false, "button"}],
      data,
      input
    )
  end

  def run_simulation([], _, acc), do: acc

  def run_simulation([{name, pulse, from} | rest], data, acc) do
    acc = update_in(acc, [:pulses, pulse], fn old -> old + 1 end)
    # acc =
    #   if name == "rx" do
    #     update_in(acc, [:pulses, :rx], fn old -> old + 1 end)
    #   else
    #     acc
    #   end
    module = data.modules[name]

    if module == nil do
      IO.inspect(name)
    end

    case module.type do
      :normal ->
        run_simulation(rest, data, acc)

      :broadcaster ->
        rest =
          rest ++
            Enum.map(module.outputs, fn out_name -> {out_name, pulse, name} end)

        run_simulation(rest, data, acc)

      :flip_flop ->
        case pulse do
          true ->
            run_simulation(rest, data, acc)

          false ->
            acc = update_in(acc, [:state, name], fn old -> !old end)
            out_pulse = acc.state[name]

            rest =
              rest ++
                Enum.map(module.outputs, fn out_name -> {out_name, out_pulse, name} end)

            run_simulation(rest, data, acc)
        end

      :conjunctor ->
        acc =
          update_in(acc, [:state, name], fn old ->
            Map.update(old, from, :not_used, fn _old -> pulse end)
          end)

        out_pulse =
          acc.state[name]
          |> Map.values()
          |> Enum.all?()
          |> then(&(not &1))

        # IO.inspect("Conjunctor out pulse: #{out_pulse}")
        rest =
          rest ++
            Enum.map(module.outputs, fn out_name -> {out_name, out_pulse, name} end)

        acc =
          if Map.has_key?(data, :last) && data.last == name do
            Map.update(acc, :last_pulses, [out_pulse], fn outs -> outs ++ [out_pulse] end)
          else
            acc
          end

        run_simulation(rest, data, acc)
    end
  end

  def simulate_n(0, state, _), do: state

  def simulate_n(n, state, data) do
    simulate_n(n - 1, simulate1(state, data), data)
  end

  def simulate_n_fast(n, state, data) do
    simulate_n_fast(0, n, state, data, %{}, %{0 => state}, %{false => 0, true => 0})
  end

  def simulate_n_fast(n, n, state, _, _, _, total_pulses) do
    IO.puts("Found no cycles")
    %{state | pulses: total_pulses}
  end

  def simulate_n_fast(idx, n, state, data, state_cache, idx_cache, total_pulses) do
    case Map.get(state_cache, state.state) do
      nil ->
        new_state = simulate1(%{state | pulses: %{false => 0, true => 0}}, data)

        new_total_pulses = %{
          true => total_pulses[true] + new_state.pulses[true],
          false => total_pulses[false] + new_state.pulses[false]
        }

        if Map.has_key?(data, :last) do
          if Enum.any?(new_state.last_pulses) do
            IO.puts("Positive out pulse at #{idx}")
            IO.inspect(new_state.last_pulses)
          end
        end

        new_state =
          %{new_state | pulses: new_total_pulses}
          |> Map.put(:last_pulses, [])

        new_state_cache = Map.put(state_cache, state.state, new_state)
        new_idx_cache = Map.put(idx_cache, idx + 1, new_state)

        simulate_n_fast(
          idx + 1,
          n,
          new_state,
          data,
          new_state_cache,
          new_idx_cache,
          new_total_pulses
        )

      _new_state ->
        # Loop from 1..idx with these total pulses
        IO.inspect("Found cycle 1..#{idx}")
        full_cycles = div(n, idx)
        last_cycle = rem(n, idx)
        last_cycle_state = Map.get(idx_cache, last_cycle)

        %{
          state
          | pulses: %{
              true => total_pulses[true] * full_cycles + last_cycle_state.pulses[true],
              false => total_pulses[false] * full_cycles + last_cycle_state.pulses[false]
            }
        }
        |> Map.put(:cycle, idx)
    end
  end
end

data =
  Part1.read_input(example2)

# |> IO.inspect()
state =
  Part1.initial_state(data)

# |> IO.inspect()

Part1.simulate_n_fast(1000, state, data)
|> then(fn %{pulses: pulses} -> pulses[true] * pulses[false] end)

# 734402834 too low
# 812609846 correct
```

## Part 2

```elixir
defmodule Part2 do
  def find_cluster([], visited, _, _), do: visited

  def find_cluster(current, visited, final, %{modules: modules} = data) do
    visited = Enum.concat(visited, current) |> MapSet.new()

    next =
      current
      |> Enum.flat_map(fn current ->
        modules[current].outputs
        |> Enum.reject(fn next ->
          next in visited or next == final
        end)
      end)

    find_cluster(next, visited, final, data)
  end

  def cut_modules(%{modules: modules} = data, names) do
    new_modules =
      modules
      |> Enum.filter(fn {name, _} ->
        name == "broadcaster" ||
          name in names
      end)
      |> Enum.map(fn {name, module} ->
        outputs =
          module.outputs
          |> Enum.filter(fn output -> output in names end)

        {name, %{module | outputs: outputs}}
      end)
      |> Enum.into(%{})

    %{data | modules: new_modules}
  end
end

data = Part1.read_input(full)

data.modules["broadcaster"].outputs
|> Enum.map(fn start ->
  names = Part2.find_cluster([start], MapSet.new(), "hp", data)

  [last] =
    MapSet.intersection(MapSet.new(data.modules["hp"].inputs), names)
    |> Enum.to_list()

  IO.inspect(last)
  data = Part2.cut_modules(data, names)
  # {data, last}
  Map.put(data, :last, last)
end)
|> Enum.map(fn data ->
  # IO.inspect(data)
  state = Part1.initial_state(data)
  # IO.inspect(state)
  Part1.simulate_n_fast(4023, state, data)
end)
|> Enum.map(fn data -> Enum.any?(data.last_pulses) end)

# cycle 3968, positive 3966
# cycle 4022, positive 4020
# cycle 3924, positive 3922
# cycle 3918, positive 3916

# cycle 3968, positive 3966
# cycle 4022, positive 4020
# Where do they both output positive at the same time?
# Answer is X
# X = A*3968 + 3966 = B*4022 + 4020

# L = LCM(3968, 4022)
# A and B exist such that
#   L = A * 3968
#   L = B * 4022
# therefore L + 3966 and L + 4020 output positive
# therefore L - 3968 + 3966 is positive and L - 4022 + 4020 is positive,
# so L-2 outputs positive
# and this presumably generalizes to 4 numbers
```

```elixir
data =
  Part1.read_input(full)
  |> Map.put(:last, "sn")

state = Part1.initial_state(data)
Part1.simulate_n_fast(3967, state, data)
```

```elixir
defmodule LCM do
  def lcm(xs) when is_list(xs) do
    xs
    |> Enum.map(&factor/1)
    |> Enum.reduce(fn fs1, fs2 ->
      Map.merge(fs1, fs2, fn _, p1, p2 -> max(p1, p2) end)
    end)
    |> Enum.reduce(1, fn {n, p}, acc ->
      acc * n ** p
    end)
  end

  def factor(n), do: factor(n, 2) |> Enum.frequencies()

  def factor(n, i) when i <= n do
    if rem(n, i) == 0 do
      [i | factor(div(n, i), i)]
    else
      factor(n, i + 1)
    end
  end

  def factor(_, _), do: []
end

l =
  [3968, 4022, 3924, 3918]
  |> LCM.lcm()

l - 2
# |> then(& &1 - 2)

3967 * 4021 * 3923 * 3917

# I don't understand why this is 3967 * 4021 * 3923 * 3917

# 5101381443180 too low
# 5111706651262 too low
# 5111706651263 too low
```

## Visualization

```elixir
defmodule Vis do
  def draw(%{modules: modules}) do
    edges =
      modules
      |> Enum.flat_map(fn {name, %{outputs: outputs, type: type}} ->
        outputs
        |> Enum.map(fn output ->
          name =
            case type do
              :normal -> name
              :broadcaster -> name
              :flip_flop -> "#{name}[\"%#{name}\"]"
              :conjunctor -> "#{name}[\"&#{name}\"]"
            end

          "  #{name} ---> #{output}"
        end)
      end)

    "graph LR;\n#{edges |> Enum.join("\n")}"
    |> Kino.Shorts.mermaid()
  end
end

Part1.read_input(full)
|> Vis.draw()
```

```elixir
Part1.read_input(example1)
|> Vis.draw()
```

```elixir
Part1.read_input(example2)
|> Vis.draw()
```

## Debug

```elixir
example3 = Kino.Input.textarea("Test input")
```

```elixir
Part1.read_input(example3)
|> Vis.draw()
```

```elixir
data = Part1.read_input(example3)
state = Part1.initial_state(data)
Part1.simulate_n_fast(1000, state, data)
```
