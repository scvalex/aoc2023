# AoC 2023 - Day 8

```elixir
Mix.install([
  {:kino, "~> 0.11.3"}
])
```

## Helpers

```elixir
example1 = Kino.Input.textarea("Example:")
```

```elixir
example2 = Kino.Input.textarea("Example:")
```

```elixir
example3 = Kino.Input.textarea("Example:")
```

```elixir
full = Kino.Input.textarea("Full input:")
```

```elixir
defmodule Aoc do
  def read_input(%Kino.Input{} = input) do
    Kino.Input.read(input)
    |> String.split("\n\n")
    |> then(fn [dirs, network] ->
      network =
        network
        |> String.split("\n", trim: true)
        |> Enum.map(fn <<node::binary-size(3), " = (", left::binary-size(3), ", ",
                         right::binary-size(3), ")">> ->
          {node, %{?L => left, ?R => right}}
        end)
        |> Enum.into(%{})

      dirs = dirs |> String.to_charlist() |> List.to_tuple()
      %{dirs: dirs, network: network}
    end)
  end
end
```

## Part 1

```elixir
defmodule Part1 do
  def follow_dirs(node, dirs, %{} = network, len, end?) when is_binary(node) do
    if end?.(node) do
      len
    else
      dir = elem(dirs, rem(len, tuple_size(dirs)))
      follow_dirs(network[node][dir], dirs, network, len + 1, end?)
    end
  end
end

data = Aoc.read_input(full)

Part1.follow_dirs(
  "AAA",
  data.dirs,
  data.network,
  0,
  &(&1 == "ZZZ")
)
```

## Part 2

```elixir
defmodule Part2 do
  def factor(n), do: factor(n, 2) |> Enum.frequencies()

  def factor(n, i) when i <= n do
    if rem(n, i) == 0 do
      [i | factor(div(n, i), i)]
    else
      factor(n, i + 1)
    end
  end

  def factor(_, _), do: []
end

%{network: network, dirs: dirs} = Aoc.read_input(full)
starts = network |> Map.keys() |> Enum.filter(&String.ends_with?(&1, "A"))

starts
|> Enum.map(fn start ->
  Part1.follow_dirs(start, dirs, network, 0, &String.ends_with?(&1, "Z"))
end)
|> Enum.map(&Part2.factor/1)
|> Enum.reduce(fn fs1, fs2 ->
  Map.merge(fs1, fs2, fn _, p1, p2 -> min(p1, p2) end)
end)
|> Enum.reduce(1, fn {num, pow}, acc -> acc * num ** pow end)
```

<!-- livebook:{"branch_parent_index":0} -->

## Experiment

```elixir
# 11A --> 5 steps --> *Z
# 22A --> 7 steps --> *Z
# 5 * 7 = 35 steps

# 11A --> 4 steps --> *Z
# 22A --> 8 steps --> *Z 
# not 4 * 8, but 8

# So, we stop at lowest-common-multiple(path1_len, path2_len, ...)
```

```elixir
"RL"
|> String.to_charlist()
|> List.to_tuple()
|> tuple_size()
```

```elixir
nodes =
  Aoc.read_input(example1).network
  |> Enum.flat_map(fn {node, %{?L => left, ?R => right}} ->
    ["  #{node}-->|L|#{left};", "  #{node}-->|R|#{right};"]
  end)
  |> Enum.join("\n")

"%%{init: {'theme': 'light'} }%%\ngraph TD;#{nodes}"
|> Kino.Shorts.mermaid()
```
